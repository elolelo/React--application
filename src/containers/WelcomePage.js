import React, { Component } from 'react';
import {  Link } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import AccountCircle from '@material-ui/icons/AccountCircle';
import axios from 'axios';

const styles = {
  card: {
    width: '50%',
    margin: '30%',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 18,
  },
  pos: {
    marginBottom: 12,
  },
};


class WelcomePage extends React.Component {

   constructor(props){
     super(props);

     this.state = {
        login: '',

        logInError:false,

        username:'',
        firstname:'',
        lastname:'',
     }
   }

   login(){
     //console.log(this.props)
     
     
     axios.get('http://localhost:8080/api/user/'+ this.state.login)
      .then(function (response) {
        console.log(response.data);
        this.props.history.push('/dashboard')

      }.bind(this))
      .catch(function (error) {
        console.log(error.response);
        this.setState({logInError: true});
      }.bind(this));
  }

  signup(){
      console.log(this.state.firstname);  // prints out firstname
      console.log(this.state.lastname);  // prints out lastname
      console.log(this.state.username);  // prints out username
  }

  handleTextInputs(event) {
    this.setState({login: event.target.value})
  }

  
  textChangeHandler(event){
    this.setState({
      [event.target.name]: event.target.value
    })
    
  }

  render() {
    const { classes } = this.props;
    return (
      <div className="WelcomePage">

      <AppBar position="static" color='primary'>
        <Toolbar variant="dense">

          <Typography variant="h4" color="inherit">
            React JS To-Do application :)    Welcome!
          </Typography>
        </Toolbar>
      </AppBar>
          <Grid container spacing={24}>
              <Grid item xs={6}>
                  <Card className={classes.card}>
                        <CardHeader  title="Login" />
                        <CardContent>
                        <FormControl className={classes.margin}>
                              <InputLabel  htmlFor="input-with-icon-adornment">Username</InputLabel>
                              <Input
                                value={this.state.login}
                                onChange={this.handleTextInputs.bind(this)}

                                id="input-with-icon-adornment"
                                startAdornment={
                                  <InputAdornment position="start">
                                    <AccountCircle />
                                  </InputAdornment>
                                }
                              />
                              {
                                this.state.logInError? <p style={{color:'red'}}>Username is incorrect</p> : <p></p>
                              }
                              
                            </FormControl>
                        </CardContent>
                        <CardActions>
                              <Button onClick={this.login.bind(this)} variant="contained" color="primary">Login</Button>
                        </CardActions>
                    </Card>
            </Grid>
          <Grid item xs={6}>
                  <Card className={classes.card}>
                        <CardHeader title="Sign up" />
                        <CardContent>
                        <FormControl className={classes.margin}>
                              <InputLabel htmlFor="input-with-icon-adornment">First Name</InputLabel>
                              <Input
                                name="firstname"
                                value={this.state.firstname}
                                onChange={this.textChangeHandler.bind(this)}
                                id="input-with-icon-adornment"
                                startAdornment={
                                  <InputAdornment position="start">
                                    <AccountCircle />
                                  </InputAdornment>
                                }
                              />
                            </FormControl>
                            <FormControl className={classes.margin}>
                              <InputLabel htmlFor="input-with-icon-adornment">Last Name</InputLabel>
                              <Input
                                name="lastname"
                                value ={this.state.lastname}
                                onChange={this.textChangeHandler.bind(this)}
                                id="input-with-icon-adornment"
                                startAdornment={
                                  <InputAdornment position="start">
                                    <AccountCircle />
                                  </InputAdornment>
                                }
                              />
                            </FormControl>
                        </CardContent>
                        <FormControl className={classes.margin}>
                              <InputLabel htmlFor="input-with-icon-adornment">Username</InputLabel>
                              <Input
                                name="username"
                                value={this.state.username}
                                onChange={this.textChangeHandler.bind(this)}
                                id="input-with-icon-adornment"
                                startAdornment={
                                  <InputAdornment position="start">
                                    <AccountCircle />
                                  </InputAdornment>
                                }
                              />
                            </FormControl>

                        <CardActions position="centre">
                              <Button onClick={this.signup.bind(this)}   variant="contained" color="primary">Sign up</Button>
                        </CardActions>
                    </Card>

          </Grid>

      </Grid>
      </div>
    );
  }
}
export default withStyles(styles)(WelcomePage);


