import React from 'react';
import CustomizedTable from '../components/CustomizedTable';
import PrimarySearchBar from '../components/PrimarySearchBar';


class DashBoard extends React.Component {
  render() {
    return (
      <div>

        {/*  This is where I will be calling all the components from the other files  */}

          <PrimarySearchBar/>
         <CustomizedTable />


      </div>
    );
  }
}


export default DashBoard;