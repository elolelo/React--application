import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';
import WelcomePage from './containers/WelcomePage';
import DashBoard from './containers/DashBoard';


class App extends Component {
  render() {
    return (
      <div className="App">
          <Router>
            <Route exact path="/" component={WelcomePage} />
            <Route path="/dashboard" component={DashBoard} />
          </Router>
      </div>
    );
  }
}
export default App;
